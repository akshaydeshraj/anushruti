package in.co.sdslabs.anushruti;

import java.io.IOException;
import java.util.Calendar;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.util.Log;
import android.widget.RemoteViews;

public class DisplayWidget extends AppWidgetProvider {
	
	 //This is activity to display the widget
	
	int awID;
	RemoteViews v;
	String quote; //Holds quote to display the spring
	int qNumber ; //Holds the number of quote to display
	int day ;	  //Holds the day of the week 
	int lastUpdate ;    //Holds the day this app was last updated
	SharedPreferences quoteData , calData; 
	SharedPreferences.Editor editor , editor_cal;
 	String radio = "QuoteString" , calendar = "CalenderString";
    Intent intent; 
	Calendar calendar1;
	
	//This method is called every time the app updates
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// TODO Auto-generated method stub
 		super.onUpdate(context, appWidgetManager, appWidgetIds);
 		
 		//Initialise database
		DatabaseHelper dbHelper = new DatabaseHelper (context);
		try {
			dbHelper.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
		try{
			dbHelper.openDataBase();
		} catch(SQLException sqle){
				throw sqle;
		}
		
     	quoteData = context.getSharedPreferences(radio, 0);

		
		final int N = appWidgetIds.length;
		Log.i("WIDGET" , "appWidgetIds : "+N);
		for (int i = 0; i < N; i++) {
			 awID = appWidgetIds[i];
			v = new RemoteViews(context.getPackageName(),
					R.layout.activity_display_widget);
			
			//Initialization of calendar variables
			calData = context.getSharedPreferences(calendar, 0);
			editor_cal = calData.edit();
			lastUpdate = calData.getInt("nCal", -1);   
			Log.i("WIDGET"  , "lastUpdate:"+lastUpdate);
			calendar1 = Calendar.getInstance();
			day = calendar1.get(Calendar.DAY_OF_WEEK);
			Log.i("WIDGET" , "Day :"+day);
			

	     	editor = quoteData.edit();	
	 		int flag = quoteData.getInt("nQuote", -1);
	 		Log.i("WIDGET" , "flag:"+flag);
	 		if(flag==-1){
	     	editor.putInt("nQuote", 0);
			editor.commit();
	 		}
	 		
	 		qNumber = quoteData.getInt("nQuote", -1);
	 		Log.i("WIDGET" , "qNumber before update:"+qNumber);
	 		
			if(lastUpdate==-1){
				editor_cal.putInt("nCal", day);
				editor_cal.commit();
			}else {
				if(lastUpdate!=day){
					int newvar=qNumber+1;
		 			if(newvar>(dbHelper.maxQuotes-1)){
		 				newvar=0;
		 			}
		 			editor.putInt("nQuote", newvar);
		 			editor.commit();
		 			editor_cal.putInt("nCal", day); //Update value of lastIpdate variable
	 				editor_cal.commit();
				}
			}
			
			qNumber = quoteData.getInt("nQuote", -1);

			//Create an Intent to Launch MainActivity
			intent = new Intent(context , MainActivity.class);		
			intent.putExtra("StringClass", "DisplayActivity");
			Log.i("WIDGET" , "qNumber sent :"+qNumber);
			intent.putExtra("Flag", qNumber);
			Log.i("WIDGET" , "putFlag : "+qNumber);
			PendingIntent pendingIntent = PendingIntent.getActivity(context,0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			Log.i("WIDGET", "qNumber after update : "+qNumber);
			
			String quote=dbHelper.getQuote(qNumber);
			//String quote = dbHelper.getQuote(15);
			//Use this to display longest quote in db
			//For testing purposes
			Log.i("WIDGET" , "qoute:"+quote);
			
			v.setTextViewText(R.id.tvWidget, quote);

			v.setOnClickPendingIntent(R.id.tvWidget, pendingIntent);
			
			appWidgetManager.updateAppWidget(awID, v);
		}
		dbHelper.close();
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onDeleted(context, appWidgetIds);
		//Toast.makeText(context, "See you soon !!", Toast.LENGTH_SHORT).show();
	}


}