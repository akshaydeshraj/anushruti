package in.co.sdslabs.anushruti;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener{

	TextView tvQuote;
	Button prev , next;
	DatabaseHelper dbHelper ;
	String display_quote;
	int qNumber;
	int flag;
	String quote;  // The quotation that will be displayed
	
	//Variables related to Shared Prefs
	SharedPreferences quoteDataApp;
 	String radio = "QuoteStringApp";
 	SharedPreferences.Editor editorApp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		tvQuote=(TextView)findViewById(R.id.tv_quote);
		prev = (Button)findViewById(R.id.bprevious);
		next = (Button)findViewById(R.id.bnext);
		
		prev.setOnClickListener(this);
		next.setOnClickListener(this);
		
		dbHelper = new DatabaseHelper(this);
		try{
			dbHelper.createDataBase();
			} catch (IOException ioe) {
				throw new Error("Unable to create database");}
 
		try{
				dbHelper.openDataBase();
			}catch(SQLException sqle) {
				throw sqle;
			}
		dbHelper.openDataBase();
				
		// qNumber must start from 0 and not 1 
		// qNumber = 0;
		
		quoteDataApp = getSharedPreferences(radio, 0);
		editorApp = quoteDataApp.edit();
		flag = quoteDataApp.getInt("nQuote", -1);
		Log.i("flag" , flag+"");
 		if(flag==-1){    //This means that there is no nQuote variable
     	editorApp.putInt("nQuote", 0);
		editorApp.commit();
 		}
 		
 		String strData;
		Intent intent = this.getIntent();
		if(intent!=null){
			strData = intent.getExtras().getString("StringClass");
			if(strData.equals("DisplayActivity")){
				qNumber = intent.getIntExtra("Flag", 0);
				Log.i("WIDGET" , "qNumber From Widget : "+qNumber);
			}else{
				qNumber=quoteDataApp.getInt("nQuote", -1);
			}
			Log.i("IntentCheck","Not Null");
			Log.i("qNumber" , "qNumber : "+qNumber);
			
		}else{
			qNumber=quoteDataApp.getInt("nQuote", -1);
			Log.i("IntentCheck","Null");
			Log.i("qNumber" , "qNumber : "+qNumber);
		}
		quote = dbHelper.getQuote(qNumber);
		tvQuote.setText(quote);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		
		quoteDataApp = getSharedPreferences(radio, 0);
		editorApp = quoteDataApp.edit();
		
		switch(v.getId()){
		
		case R.id.bprevious :
			qNumber --;
			if(qNumber<0){
				qNumber = dbHelper.maxQuotes-1;	
				Log.i("qNumber",""+qNumber);
			}
			quote = dbHelper.getQuote(qNumber);
			tvQuote.setText(quote);
			break;
		
		case R.id.bnext :
			qNumber++;
			if(qNumber>(dbHelper.maxQuotes-1)){
				qNumber=0;
			}
			quote = dbHelper.getQuote(qNumber);
			tvQuote.setText(quote);
			break;
		}
		editorApp.putInt("nQuote", qNumber);
		editorApp.commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		super.onOptionsItemSelected(item);
		
		switch(item.getItemId()){
		
		case R.id.about_activity :
			Intent intent = new Intent(this , AboutUs.class);
			startActivity(intent);
			break;
		}
	return true;
	}



}
