//package in.co.sdslabs.anushruti;
//
//import java.io.IOException;
//
//import android.app.Activity;
//import android.appwidget.AppWidgetManager;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.database.SQLException;
//import android.os.Bundle;
//import android.util.Log;
//import android.widget.RemoteViews;
//
//public class WidgetConfig extends Activity {
//	
//	//This is the activity to configure our widget
//	
//	AppWidgetManager awm;
//	Context c;
//	int awID;
//	SharedPreferences somedata;
// 	String radio = "SharedString";
// 	SharedPreferences.Editor editor;
// 	int qNumber , flag;
// 	
// 	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
//		DatabaseHelper dbHelper ;
//		dbHelper = new DatabaseHelper(this);
//		Log.i("WidgetConfig","Number1");
//		try {
//			dbHelper.createDataBase();
//		} catch (IOException ioe) {
//			throw new Error("Unable to create database");
//		}
// 
//		try{
//			dbHelper.openDataBase();
//		} catch(SQLException sqle){
//				throw sqle;
//		}
//				
//		Log.i("WidgetConfig","Number2");
//		c = WidgetConfig.this;
//		Intent in = getIntent();
//		Bundle extras = in.getExtras();
//		if (extras != null) {
//			awID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
//					AppWidgetManager.INVALID_APPWIDGET_ID);
//			// Toast.makeText(c, "extra!=null", Toast.LENGTH_SHORT).show();
//		}
//		awm = AppWidgetManager.getInstance(c);
//		RemoteViews v = new RemoteViews(c.getPackageName(), R.layout.activity_display_widget);
//		Log.i("WidgetConfig","Number3");
//		somedata = getSharedPreferences(radio, 0);
//		editor = somedata.edit();
//		flag = somedata.getInt("nQuote", -1);
//		Log.i("flag" , flag+"");
// 		if(flag==-1){    //This means that there is no nQuote variable
//     	editor.putInt("nQuote", 0);
//		editor.commit();
// 		}
// 		Log.i("WidgetConfig","Number4");
//		qNumber=somedata.getInt("nQuote", -1);
//	
//		String quote=dbHelper.getQuote(qNumber);
//		v.setTextViewText(R.id.tvWidget, quote);
//		Log.i("WidgetConfig","Number5");
//		//Update the shared preferances
//		
//		int newvar=qNumber+1;
//			if(newvar>(dbHelper.maxQuotes-1)){
//				newvar=0;
//			}
//			editor.putInt("nQuote", newvar);
//			editor.commit();
//			Log.i("WidgetConfig","Number6");
//		awm.updateAppWidget(awID, v);
//		Intent result = new Intent();
//		result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
//		setResult(RESULT_OK, result);
//		dbHelper.close();
//		finish();
//	}
//}
