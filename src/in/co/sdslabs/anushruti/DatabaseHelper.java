package in.co.sdslabs.anushruti;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHelper extends SQLiteOpenHelper{
	
	//In the database run the query [CREATE TABLE "android_metadata("locale"TEXT DEFAULT 'en_US')]
	
	@SuppressLint("SdCardPath")
	private static String DB_PATH = "/data/data/in.co.sdslabs.anushruti/databases/";
	private static final String DB_NAME = "anushruti.db" ;
	
	private SQLiteDatabase myDataBase; 
	private final Context myContext;
	
	//Variable for number of quotes in the database
	public final int maxQuotes = 205;
	
	public DatabaseHelper(Context context) {
		
		super(context, DB_NAME, null, 1);
        this.myContext = context;
	}
	
	
	public void createDataBase() throws IOException{
		 boolean dbExist = checkDataBase();
		 if(dbExist){
			 return;
		 }
		 else{
			 	this.getReadableDatabase();
			 	try
			 	{
			 		copyDataBase();
			 	}catch(IOException e) {
			 		Log.i("DEBUG",  "error copying db");
				 	throw new Error("Error copying database");
			 	}
			 }
		 }
	
	
	private boolean checkDataBase(){
		 
    	SQLiteDatabase checkDB = null;
    	try
    	{
    		String myPath = DB_PATH + DB_NAME;
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    	}
    	catch(SQLiteException e){
    	}
    	if(checkDB != null) checkDB.close();
    	return checkDB!= null?true:false;
    }
	
	
	private void copyDataBase() throws IOException{
		 
    	//Open your local db as the input stream
    	InputStream myInput = myContext.getAssets().open(DB_NAME);
 
    	// Path to the just created empty db
    	String outFileName = DB_PATH + DB_NAME;
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
	
	
	public void openDataBase() throws SQLException{
	        String myPath = DB_PATH + DB_NAME;
	    	myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
	 	}
	
	
	    @Override
		public synchronized void close() {
	    	if(myDataBase != null) myDataBase.close();
	    	super.close();
	    }
	
	    
	    //Add custom function to return quotation after reading day no. from shared preferance
	    
	public String getQuote(int q_number){
	    	
	    	SQLiteDatabase db = this.getReadableDatabase();
	    	Cursor cursor = db.rawQuery("SELECT * FROM thoughts_table WHERE 1", null);
	    	String data[][] = new String[cursor.getCount()][cursor.getColumnCount()];
	    	
	    	if (cursor != null) {
				Log.i("DEBUG","Coloumn count :"+cursor.getColumnCount());
		        int i = 0;
		        while (cursor.moveToNext()) {
		            int j = 0;
		            while (j < cursor.getColumnCount()) {
		                data[i][j] = cursor.getString(j);
		                Log.i("DEBUG","data["+i+"]["+j+"]: "+data[i][j]);
		                j++;
		            }
		            i++;
		        }
		        cursor.close();
		    }
	    	Log.v("DEBUG",data[q_number][1]);
	    	return data[q_number][1];
	    }

	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}	

	
}
